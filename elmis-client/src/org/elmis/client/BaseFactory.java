package org.elmis.client;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.http.conn.HttpHostConnectException;
import org.elmis.utils.HttpClient;
import org.elmis.utils.ResponseEntity;
import us.monoid.json.JSONArray;
import us.monoid.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * PoC
 * Created by: Elias Muluneh
 * Date: 7/1/13
 * Time: 5:11 PM
 */
public class BaseFactory {

    // TODO: remove the hardcoded default values.
    public static String BaseURL = "http://uat.tz.elmis-dev.org";

    public static String Vendor = "openLmis";

    public static String Authorization  = "d7d9b0a9-27e8-417a-8456-9ca93c90618d";

    public static String UserName = "Admin123";

    public static Long FacilityID = 1L;

    public static Object loadJSONLookup(String service, Class<?> type) throws Exception{

        HttpClient client = new HttpClient();
        client.createContext();

        ResponseEntity response = client.SendJSON("{}", BaseURL + "/rest-api/lookup/" + service, HttpClient.POST, Vendor , Authorization );
        try{
            JSONObject array = new JSONObject(response.getResponse());
            JSONArray innerArray = array.getJSONArray(service);
            Gson gson = new Gson();

            List list = new ArrayList<>() ;
            for(int i = 0; i < innerArray.length(); i++){
                list.add(gson.fromJson(innerArray.getJSONObject(i).toString(),type)) ;
            }
            return list;
        }

        catch(Exception ex){

            if(ex instanceof NullPointerException){
                throw new Exception ("Network Error");
            }
            throw new Exception ("Authentication Error");
        }

    }


    public static Object uploadJSON(String service, String result , String json , Class<?> type) throws Exception{

        HttpClient client = new HttpClient();
        client.createContext();

        ResponseEntity response = client.SendJSON(json, BaseURL + "/rest-api/" + service, HttpClient.POST, Vendor , Authorization );

        JSONObject array = new JSONObject(response.getResponse());

        String errorObject = null;
        try{
            errorObject = array.getString("error");
        }catch(Exception ex){

        }
        if(errorObject != null){
            throw new Exception(errorObject);
        }
        try{
            JSONObject resultJson = array.getJSONObject(result);
            Gson gson = new Gson();
            return gson.fromJson(resultJson.toString(),type);
        }
        catch(Exception ex){

            if(ex instanceof NullPointerException){
                throw new Exception ("Network Error");
            }
            // JSON was not being parsed,
            // this indicates that the authentication has failed.
            throw new Exception("Authentication Error");
        }

    }


}
