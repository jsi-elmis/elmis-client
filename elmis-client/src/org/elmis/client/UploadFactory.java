package org.elmis.client;

import com.google.gson.Gson;
import org.elmis.model.rnr.Report;

/**
 * PoC
 * Created by: Elias Muluneh
 * Date: 7/2/13
 * Time: 1:24 AM
 */
public class UploadFactory extends BaseFactory {

    public static Long UploadRnR(Report report) throws Exception{
        Gson gson = new Gson();
        String json = gson.toJson(report,report.getClass());
        return (Long)uploadJSON("requisitions","R&R", json,Long.class );
    }
}
