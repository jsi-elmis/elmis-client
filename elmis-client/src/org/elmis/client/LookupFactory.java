package org.elmis.client;

import org.elmis.model.lookup.*;

import java.util.ArrayList;

/**
 * PoC
 * Created by: Elias Muluneh
 * Date: 7/1/13
 * Time: 5:08 PM
 */
public class LookupFactory extends BaseFactory {

    public static ArrayList<DosageUnit> getDosageUnits() throws Exception{
        return (ArrayList<DosageUnit>) loadJSONLookup("dosage-units", DosageUnit.class);
    }



    public static ArrayList<Facility> getFacilities() throws Exception{
        return (ArrayList<Facility>) loadJSONLookup("facilities", Facility.class);
    }

    public static ArrayList<FacilityType> getFacilityTypes() throws Exception{
        return (ArrayList<FacilityType>) loadJSONLookup("facility-types", FacilityType.class);
    }


    public static ArrayList<Product> getProducts() throws Exception{
        return (ArrayList<Product>) loadJSONLookup("products", Product.class);
    }

    public static ArrayList<ProductCategory> getProductCategories() throws Exception{
        return (ArrayList<ProductCategory>) loadJSONLookup("product-categories", ProductCategory.class);
    }

    public static ArrayList<Program> getPrograms() throws Exception{
        return (ArrayList<Program>) loadJSONLookup("programs", Program.class);
    }

    public static ArrayList<ProcessingPeriod> getProcessingPeriods() throws Exception{
        return (ArrayList<ProcessingPeriod>) loadJSONLookup("processing-periods", ProcessingPeriod.class);
    }

    public static ArrayList<LossesAndAdjustmentsType> getLossesAndAdjustmentTypes() throws Exception{
        return (ArrayList<LossesAndAdjustmentsType>) loadJSONLookup("losses-adjustments-types", LossesAndAdjustmentsType.class);
    }
}
