package org.elmis.model.rnr;

/**
 * PoC
 * Created by: Elias Muluneh
 * Date: 7/2/13
 * Time: 12:58 AM
 */

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;


@Data
@NoArgsConstructor
public class RnrLineItem  {

    private String productCode;

    private Integer beginningBalance;
    private Integer quantityReceived;
    private Integer quantityDispensed;
    private List<LossesAndAdjustments> lossesAndAdjustments = new ArrayList<>();

    private Integer stockInHand;
    private Integer stockOutDays;
    private Integer newPatientCount;
    private Integer quantityRequested;
    private String reasonForRequestedQuantity;

    private Integer amc;
    private Integer normalizedConsumption;
    private Integer calculatedOrderQuantity;
    private Integer maxStockQuantity;

    private Integer quantityApproved;
    private String remarks;


}

