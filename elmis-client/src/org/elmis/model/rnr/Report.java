package org.elmis.model.rnr;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class Report {
    private Long requisitionId;
    private Long facilityId;
    private Long programId;
    private Long periodId;
    private String userId;
    private Vendor vendor;
    private List<RnrLineItem> products;

    public void validate() throws Exception {
        if (facilityId == null || programId == null || periodId == null || userId == null || vendor == null) {
            throw new Exception("error.restapi.mandatory.missing");
        }
    }


}
