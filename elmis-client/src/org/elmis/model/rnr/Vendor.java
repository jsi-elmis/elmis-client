package org.elmis.model.rnr;

/**
 * PoC
 * Created by: Elias Muluneh
 * Date: 7/2/13
 * Time: 1:10 AM
 */

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class Vendor{

    private String name;
    private Boolean active;
    private String authToken;
}