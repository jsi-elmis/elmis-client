package org.elmis.model.rnr;

/**
 * PoC
 * Created by: Elias Muluneh
 * Date: 7/2/13
 * Time: 1:04 AM
 */

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.elmis.model.lookup.LossesAndAdjustmentsType;


@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class LossesAndAdjustments  {

    public LossesAndAdjustmentsType type;
    private Integer quantity;
}