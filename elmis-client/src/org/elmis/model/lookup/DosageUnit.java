package org.elmis.model.lookup;

import lombok.Data;


@Data
public class DosageUnit {
    private Integer id;
    private Integer displayorder;
    private String code;



}