package org.elmis.model.lookup;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * PoC
 * Created by: Elias Muluneh
 * Date: 7/3/13
 * Time: 9:26 AM
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProcessingPeriod {
    private Integer id;
    private String name;
    private String description;
    //private Date startdate;
    //private Date enddate;
}