package org.elmis.model.lookup;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * PoC
 * Created by: Elias Muluneh
 * Date: 7/3/13
 * Time: 9:25 AM
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Program {
    private Integer id;
    private String name;
    private String code;
    private String description;

}
