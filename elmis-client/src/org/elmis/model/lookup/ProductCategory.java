package org.elmis.model.lookup;

import lombok.Data;

/**
 * PoC
 * Created by: Elias Muluneh
 * Date: 7/1/13
 * Time: 5:27 PM
 */
@Data
public class ProductCategory {

    private Integer id;
    private String name;
    private String code;

}
