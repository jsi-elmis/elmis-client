package org.elmis.model.lookup;

/**
 * PoC
 * Created by: Elias Muluneh
 * Date: 7/3/13
 * Time: 9:24 AM
 */

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FacilityType {
    private Integer id;
    private String name;
}