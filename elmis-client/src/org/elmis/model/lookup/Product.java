package org.elmis.model.lookup;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * PoC
 * Created by: Elias Muluneh
 * Date: 7/1/13
 * Time: 5:26 PM
 */
@Data
@NoArgsConstructor
public class Product {

    private boolean active;
    private Integer categoryid;
    private String code;
    private String dispensingunit;
    private Integer displayorder ;
    private Integer dosageunitid ;
    private Integer formid;
    private String  fullname;
    private Boolean fullsupply;
    private Integer packsize;
    private String strength;
    private Boolean tracer;
    private String  type;
    private Integer packroundingthreshold;

}
