import org.elmis.client.BaseFactory;
import org.elmis.client.LookupFactory;
import org.elmis.client.UploadFactory;
import org.elmis.model.lookup.*;
import org.elmis.model.rnr.*;
import org.elmis.utils.StringUtils;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;

public class Main extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JButton btnLoadDosageUnit;
    private JButton loadProductCategoriesButton;
    private JTabbedPane tabbedPane1;
    private JTextField txtVendorName;
    private JTextField txtBaseURL;
    private JTextField txtAuthentication;
    private JButton saveSettingsButton;
    private JButton loadProductsButton;
    private JTextArea textLookups;
    private JButton submitReportsButton;
    private JTextArea textRequisitonStatus;
    private JTextField txtUserName;
    private JTextField txtFacilityID;
    private JButton facilityTypesButton;
    private JButton facilitiesButton;
    private JButton programsButton;
    private JButton lossesAndAdjustmentTypesButton;
    private JButton processingPeriodsButton;
    private JTextField textProgramID;
    private JTextField textProcessingPeriodID;
    private JTextField textProducts;

    public Main() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);


// call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                  onCancel();
            }
        });




// call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        btnLoadDosageUnit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                try{
                    // Call the web service and convert the result to array of Dosage Units.
                    // This is all it takes to call the web service (No complicated piece of code)
                    // I have tried to encapsulate all the code requried
                    ArrayList<DosageUnit> units =  LookupFactory.getDosageUnits();

                    // print the result on the cosole
                    // clear the console
                    textLookups.setText("Start Loading Dosage Units from " + BaseFactory.BaseURL + "\n");

                    for(int i = 0; i < units.size(); i++){
                        DosageUnit unit = (DosageUnit) units.get(i);
                        textLookups.append("\n" + unit.getId() + " - " + unit.getCode());
                    }

                }catch(Exception ex){
                     System.out.println(ex.getMessage());
                }
            }
        });

        loadProductCategoriesButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                try{
                    ArrayList<ProductCategory> categories =  LookupFactory.getProductCategories();

                    textLookups.setText("Start Loading Product Categories from " + BaseFactory.BaseURL + "\n") ;
                    for(int i = 0; i< categories.size(); i++){
                        ProductCategory category = (ProductCategory) categories.get(i);
                        textLookups.append("\n" + StringUtils.padRight(category.getId().toString() , 3) + " - " + StringUtils.padRight( category.getCode(),7) + " - " + category.getName());
                    }
                }catch(Exception ex){
                    System.out.println(ex.getMessage());
                }
            }
        });

        loadProductsButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                try{
                    ArrayList<Product> array =  LookupFactory.getProducts();

                    textLookups.setText("Start Loading Products from " + BaseFactory.BaseURL + "\n") ;
                    for(int i = 0; i < array.size() && i < 30 ; i++){
                        Product item = (Product) array.get(i);
                        textLookups.append("\n" + (i + 1) + " - " + StringUtils.padRight(item.getFullname(),30 )+ " - " + StringUtils.padRight(item.getCode(),7) + " - " + item.getCategoryid());
                    }
                }catch(Exception ex){
                    textLookups.setText(ex.getMessage());
                }
            }
        });
        submitReportsButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
               try{

                   textRequisitonStatus.setText("Uploading RnR to " + BaseFactory.BaseURL +" \n");
                   org.elmis.model.rnr.Report report = new org.elmis.model.rnr.Report();
                   report.setProgramId(Long.parseLong(textProgramID.getText()));
                   report.setPeriodId(Long.parseLong(textProcessingPeriodID.getText()));

                   report.setFacilityId(BaseFactory.FacilityID);
                   report.setUserId(BaseFactory.UserName);
                   report.setVendor(new Vendor());
                   report.getVendor().setName(BaseFactory.Vendor);
                   report.getVendor().setAuthToken(BaseFactory.Authorization);



                   report.setProducts( new ArrayList<RnrLineItem>());

                   // split the products from the input box
                   String[] productCodes = textProducts.getText().split(",");
                   for(int i = 0; i < productCodes.length; i++){
                       RnrLineItem item = new RnrLineItem();
                       item.setProductCode(productCodes[i]);

                       item.setBeginningBalance(1);
                       item.setQuantityReceived(1);
                       item.setQuantityDispensed(1);
                       item.setQuantityRequested(1);
                       item.setStockInHand(1);

                       report.getProducts().add(item);
                   }

                   // now send the rnr to the server.
                   textRequisitonStatus.append("Successful upload - " + UploadFactory.UploadRnR(report).toString());


               } catch(Exception ex){
                   System.out.println(ex.getMessage());
                   textRequisitonStatus.append(ex.getMessage());
               }
            }
        });

        saveSettingsButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                 // read the UI values and set those to the factory methods.

                BaseFactory.BaseURL =  txtBaseURL.getText();
                BaseFactory.Vendor = txtVendorName.getText();
                BaseFactory.Authorization = txtAuthentication.getText();
                BaseFactory.UserName = txtUserName.getText();
                BaseFactory.FacilityID = Long.parseLong(txtFacilityID.getText());

            }
        });

        facilityTypesButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try{
                    ArrayList<FacilityType> array =  LookupFactory.getFacilityTypes();

                    textLookups.setText("Start Loading Facility Types from " + BaseFactory.BaseURL + "\n") ;
                    for(int i = 0; i < array.size() && i < 30 ; i++){
                        FacilityType item = (FacilityType) array.get(i);
                        textLookups.append("\n" + StringUtils.padRight(item.getId().toString(),7 )+ " - " + StringUtils.padRight(item.getName(), 30) );
                    }
                }catch(Exception ex){
                    textLookups.append("\n" + ex.getMessage());
                }
            }
        });
        facilitiesButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try{
                    ArrayList<Facility> array =  LookupFactory.getFacilities();

                    textLookups.setText("Start Loading Facilities from " + BaseFactory.BaseURL + "\n") ;
                    for(int i = 0; i < array.size() && i < 30 ; i++){
                        Facility item = (Facility) array.get(i);
                        textLookups.append("\n" + item.getId().toString() + " - " +  StringUtils.padRight(item.getCode(),7 )+ " - " + StringUtils.padRight(item.getName(), 30) );
                    }
                }catch(Exception ex){
                    textLookups.append("\n" + ex.getMessage());
                }
            }
        });
        programsButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try{
                    ArrayList<Program> array =  LookupFactory.getPrograms();

                    textLookups.setText("Start Loading Programs from " + BaseFactory.BaseURL + "\n") ;
                    for(int i = 0; i < array.size() && i < 30 ; i++){
                        Program item = (Program) array.get(i);
                        textLookups.append("\n" + StringUtils.padRight( item.getId().toString() ,7 )+ " - " + StringUtils.padRight(item.getCode(), 10) + " - " + item.getName() );
                    }
                }catch(Exception ex){
                    textLookups.append("\n" + ex.getMessage());
                }
            }
        });
        lossesAndAdjustmentTypesButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try{
                    ArrayList<LossesAndAdjustmentsType> array =  LookupFactory.getLossesAndAdjustmentTypes();

                    textLookups.setText("Start Loading Losses And Adjustements Types from " + BaseFactory.BaseURL + "\n") ;
                    for(int i = 0; i < array.size() && i < 30 ; i++){
                        LossesAndAdjustmentsType item = (LossesAndAdjustmentsType) array.get(i);
                        textLookups.append("\n" + item.getId().toString() + " - " +  StringUtils.padRight(item.getDisplayOrder().toString() , 7 )+ " - " + StringUtils.padRight(item.getName(), 30) + " - Additive = " + item.getAdditive().toString());
                    }
                }catch(Exception ex){
                    textLookups.append("\n" + ex.getMessage());
                }
            }
        });
        processingPeriodsButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try{
                    ArrayList<ProcessingPeriod> array =  LookupFactory.getProcessingPeriods();

                    textLookups.setText("Start Loading Processing Periods from " + BaseFactory.BaseURL + "\n") ;
                    for(int i = 0; i < array.size() && i < 30 ; i++){
                        ProcessingPeriod item = ( ProcessingPeriod ) array.get(i);
                        textLookups.append("\n" + item.getId().toString() + " - " + StringUtils.padRight(item.getName(),10 )+ " - " + StringUtils.padRight(item.getDescription(), 20)  );
                    }
                }catch(Exception ex){
                    textLookups.append("\n" + ex.getMessage());
                }
            }
        });
    }

    private void onCancel() {
// add your code here if necessary
        dispose();
    }

    public static void main(String[] args) {
        Main dialog = new Main();

        dialog.txtBaseURL.setText(BaseFactory.BaseURL);
        dialog.txtVendorName.setText(BaseFactory.Vendor);
        dialog.txtAuthentication.setText(BaseFactory.Authorization);
        dialog.txtUserName.setText(BaseFactory.UserName);
        dialog.txtFacilityID.setText(BaseFactory.FacilityID.toString());
        dialog.setBounds(new Rectangle(2,2,800,600));
        dialog.setVisible(true);
        System.exit(0);
    }





    private void createUIComponents() {
        // TODO: place custom component creation code here
    }
}
